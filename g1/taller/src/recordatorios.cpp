#include <iostream>
#include <vector>
using namespace std;



class Fecha {
    public:
        Fecha (int mes, int dia);
        int mes();
        int dia();
        void incrementar_dia();
    private:
        int mes_;
        int dia_;
};

Fecha :: Fecha(int mes, int dia) : mes_(mes) , dia_(dia) {}
int Fecha :: mes() {
    return mes_;
}
int Fecha :: dia() {
    return dia_;
}
void Fecha :: incrementar_dia() {
    if(mes_!=2) {
        if (dia_ > 29){
            if (mes_==4 ||mes_==6 || mes_==9 ||mes_==11){
                mes_++;
                dia_=1;
            }else{
                if(dia_==30) {
                    dia_ = 31;
                }else{
                    mes_++;
                    dia_=1;
                }
            }
        }else{
            dia_++;
        }
    }else{
        if(dia_==28){
            mes_++;
            dia_ = 0;
        }else{
            dia_++;
        }
    }
}

class Horario {
    public:
        Horario(int hora, int min);
        int hora();
        int min();
    private:
        int hora_;
        int min_;
};
Horario :: Horario(int hora, int min) : hora_(hora) , min_(min) {}
int  Horario :: hora() {
    return hora_;
}
int  Horario :: min() {
    return min_;
}

class Recordatorio {
    public:
        Recordatorio(Fecha dia, Horario hora, string comp);
        Horario horario();
        Fecha fecha();
        string compr();

    private:
        Horario horario_;
        Fecha fecha_;
        string compr_;
};

Recordatorio ::Recordatorio(Fecha dia, Horario hora, string comp) : horario_(hora) , fecha_(dia) , compr_(comp) {}
Horario Recordatorio :: horario() {
    return horario_;
}
Fecha Recordatorio :: fecha() {
    return fecha_;
}
string Recordatorio :: compr() {
    return compr_;
}


class Agenda {
    public:
        Agenda(Fecha fecha_inicial);
        void agregar_recordatorio(Recordatorio rec);
        void incrementar_dia();
        vector<Recordatorio> recordatorios_de_hoy();
        Fecha hoy();

    private:
        Fecha dia_;
        vector<Recordatorio> compromisos_;
};

Agenda :: Agenda(Fecha fecha_inicial) : dia_(fecha_inicial) { }
void Agenda :: agregar_recordatorio(Recordatorio rec) {
    compromisos_.push_back(rec);
}
void Agenda :: incrementar_dia() {
    dia_.incrementar_dia();
}
vector<Recordatorio> Agenda :: recordatorios_de_hoy() {
    int cant_recs = compromisos_.size();
    vector<Recordatorio> out;
    for(int i=0; i<cant_recs; i++){
        if(compromisos_[i].fecha().mes() == dia_.mes() && compromisos_[i].fecha().dia() == dia_.dia()){
            out.push_back(compromisos_[i]);
        }
    }
}
Fecha Agenda :: hoy() {
    return dia_;
}

bool mas_temprano(Horario a, Horario b){
    return a.hora()<b.hora() && a.min()<b.min();
}

vector<Recordatorio> ordenar_recordatorios (vector<Recordatorio> recs){
    vector<Recordatorio> out;
    int cant_rec = recs.size();
    Horario a(0,0);

    for(int i=0; i<cant_rec; i++){
        Recordatorio hold=recs[0];
        for(int j=0; j<cant_rec; j++){
            if(mas_temprano(recs[j].horario(),hold.horario()) && mas_temprano(a,recs[j].horario())) {
                hold = recs[j];
            }
        }
        a=hold.horario();
        out.push_back(hold);
    }

    return out;
}





ostream& operator<<(ostream& os, Fecha r) {
    os <<r.dia() << "/" << r.mes();
    return os;
}

ostream& operator<<(ostream& os, Horario r) {
    os <<r.hora() << ":" << r.min();
    return os;
}

ostream& operator<<(ostream& os, Recordatorio r) {
    os <<r.compr()<< " @ " << r.fecha() << " " << r.horario();
    return os;
}

ostream& operator<<(ostream& os, Agenda a) {
    os << a.hoy() << endl;
    os << "=====" << endl;
    vector<Recordatorio> ord = ordenar_recordatorios(a.recordatorios_de_hoy());
    int cant_rec = ord.size();
    for(int i=0; i<cant_rec; i++){
        os << ord[i] << endl;
    }
    return os;
}
